#!/usr/bin/make

EMACSARGS=-a "" -s don_armstrong_resume
EMACS=emacsclient


all: don_armstrong_resume.pdf

# don_armstrong_resume.pdf: don_armstrong_resume.tex $(wildcard *.bib)
# 	latexmk -pdf -pdflatex="xelatex -interaction=nonstopmode" -bibtex -use-make $<


don_armstrong_resume.pdf: don_armstrong_resume.org dlaresume.cls
	$(EMACS) $(EMACSARGS) \
	--eval "(setq org-latex-remove-logfiles nil)" \
	--eval '(with-current-buffer (find-file-noselect "$<") (org-latex-export-to-pdf) (kill-buffer))'
