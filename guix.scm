;; THIS FILE DOESN'T ACTUALLY WORK YET!
(use-modules (gnu packages tex)
             (guix build-system gnu)
             (guix build-system texlive)
             (guix git-download)
             (guix licenses)
             (guix packages)
             (guix profiles)
             (guix utils))

(define simple-texlive-package
  (@@ (gnu packages tex) simple-texlive-package))

(define texlive-latex-fontawesome
  (package
    (inherit (simple-texlive-package
              "texlive-latex-fontawesome"
              (list "/fonts/enc/dvips/fontawesome/"
                    "/fonts/map/dvips/fontawesome/"
                    "/fonts/opentype/public/fontawesome/"
                    "/fonts/tfm/public/fontawesome/"
                    "/fonts/type1/public/fontawesome/"
                    "/tex/latex/fontawesome/")
              (base32
               "1s1lkmpmb9fj4qmdhll2mn1hz8xk89z9rwga5zqpjb9was4vrg1y")
              #:trivial? #t))
    (home-page "https://ctan.org/tex-archive/macros/latex/contrib/moderncv")
    (synopsis "Foo")
    (description "Foo")
    (license lppl1.3c+)))

(define texlive-moderncv
  (package
    (inherit (simple-texlive-package
              "texlive-moderncv"
              (list "/tex/latex/moderncv/")
              (base32
               "1yapivf89vq0xwkdblr54mry7knqjhgnfqlr3n14g02ybxz7rpa9")
              #:trivial? #t))
    (propagated-inputs
     `(("texlive-etoolbox" ,texlive-etoolbox)
       ("texlive-xcolor" ,texlive-xcolor)
       ("texlive-latex-colortbl" ,texlive-latex-colortbl)
       ("texlive-generic-ifxetex" ,texlive-generic-ifxetex)
       ("texlive-latex-oberdiek" ,texlive-latex-oberdiek)
       ("texlive-url" ,texlive-url)
       ("texlive-latex-fancyhdr" ,texlive-latex-fancyhdr)
       ("texlive-latex-l3packages" ,texlive-latex-l3packages)
       ("texlive-microtype" ,texlive-microtype)
       ("texlive-latex-fontawesome" ,texlive-latex-fontawesome)
       ("texlive-latex-geometry" ,texlive-latex-geometry)
       ("texlive-latex-hyperref" ,texlive-latex-hyperref)
       ("texlive-latex-ms" ,texlive-latex-ms)
       ("texlive-latex-pgf" ,texlive-latex-pgf)
       ("texlive-amsfonts" ,texlive-amsfonts/patched)))
    (home-page "https://ctan.org/pkg/moderncv")
    (synopsis "Foo")
    (description "Foo")
    (license lppl1.3c+)))

(list (texlive-union (list texlive-amsfonts
                           texlive-base
                           texlive-cm
                           ;; texlive-cm-super
                           ;; texlive-fonts-latex
                           texlive-fonts-ec
                           texlive-latex-base
                           ;; texlive-metafont-base
                           ;; texlive-txfonts
                           ;; texlive-fonts-rsfs
                           ;; texlive-fonts-stmaryrd
                           ;; texlive-marvosym
                           texlive-tools
                           texlive-symbol
                           texlive-moderncv)))
