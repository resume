\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{dlaresume}[2017/01/21 DLA Resume Class]

\RequirePackage{ifthen}
\RequirePackage{xkeyval}

%\DeclareOptionX*{\PassOptionsToClass{\CurrentOption}}
\ProcessOptionsX*

% much of this was borrowed from Mako's work: git://projects.mako.cc/bmh-cv

% include data on fonts
\LoadClass{article}

\RequirePackage{fontspec}

\setmainfont[Ligatures=TeX]{FreeSerif}
\setsansfont{FreeSans}
\setmonofont{FreeMono}

\RequirePackage{lastpage}
\PassOptionsToPackage{letterpaper,margin=0.75in,ignoreall=true}{geometry}
\RequirePackage{geometry}
\RequirePackage{url}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\PassOptionsToPackage{final}{graphicx}
\RequirePackage{graphicx}
\PassOptionsToPackage{labelfont=bf,font=sl}{caption}
\RequirePackage{caption}
\RequirePackage{multirow}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\RequirePackage{color}
\PassOptionsToPackage{x11names,svgnames,table}{xcolor}
\RequirePackage{xcolor}
\RequirePackage{enumitem}
\RequirePackage{hyperref}
\PassOptionsToPackage{nomargin,inline}{fixme}
\RequirePackage{fixme}
\PassOptionsToPackage{capitalize}{cleveref}
\RequirePackage{cleveref}
\PassOptionsToPackage{backend=biber,hyperref=true,style=numeric-comp}{biblatex}
\RequirePackage{biblatex}
\addbibresource{references.bib}

\hypersetup{colorlinks=true, linkcolor=Black, citecolor=Black, filecolor=Black,
    urlcolor=Black, unicode=true}
\urlstyle{same}
% customize the titles so that they appear in the right margin
\PassOptionsToPackage{explicit}{titlesec}
\RequirePackage{titlesec}
% use QR codes
\RequirePackage{qrcode}

%\AtBeginDocument{
\setlength{\marginparwidth}{0in}
\setlength{\titlewidth}{0in}
% set the default indent to nothing
\setlength{\parindent}{0em}
\setlength{\parskip}{0.5em}
%}
% \makeatletter
% \newif\if@aftersec
% \newcommand\setsubskip{%
%     \global\@aftersectrue
%     \everypar{%
%         \global\@aftersecfalse
%         \if@noskipsec
%             \global\@noskipsecfalse
%             \clubpenalty\@M
%             \hskip-\parindent
%             \begingroup
%                 \@svsechd\unskip{\hspace{\@tempskipb}}%
%             \endgroup
%         \else
%             \clubpenalty\@clubpenalty\everypar{}%
%         \fi}}
% 
% \newcommand\subskip{%
%   \if@aftersec
%      \removelastskip%         EDIT 2
%      \vspace{-\baselineskip}% EDIT 2 ??????????????
%   \else
%      \removelastskip%
%      \vspace{0.0em}
%   \fi
%   \global\@aftersecfalse}
% \makeatother

\definecolor{date}{HTML}{666666} 
\definecolor{primary}{HTML}{0b0b0b} 
\definecolor{headings}{HTML}{701A1A}
\definecolor{subheadings}{HTML}{500000}

\titleformat{\section}{%
\color{headings}\fontsize{14pt}{14pt}\selectfont%
 \scshape}{}{0pt}{\textcolor{headings}{#1}}[{\titleline{\titlerule[1pt]}}]
\titlespacing*{\section}{0em}{0.5em}{0em}
\titleformat{\subsection}{%
  \color{subheadings}\fontsize{12pt}{12pt}\selectfont%
 \raggedright}{}{0pt}{\textcolor{subheadings}{#1}}
\titlespacing*{\subsection}{0em}{0.25em}{-\parskip}

\def\myauthor{Don Armstrong}
\def\mytitle{Resume}
\def\mycopyright{\myauthor}
\def\myaddress{}
\def\myemail{don@donarmstrong.com}
\def\myweb{https://www.donarmstrong.com}
\def\myphone{+1 714-813-8531}

% create a special cvlist environment to format the items
% \newenvironment{cvlist}{
%   \setlength{\topsep}{0pt}
%   \setlength{\partopsep}{0pt}
% \begin{list}{-}{\leftmargin=0em \itemindent=1.0em}
%   \setlength{\itemsep}{0pt}
%   \setlength{\parskip}{0em}
%   \setlength{\parsep}{0em}
%   \setlength{\parindent}{0em}}%
% {\end{list}}

\setlist[itemize]{noitemsep,nosep,leftmargin=1em}
\setlist[enumerate]{noitemsep,nosep,leftmargin=1em}
% 
% 
% \renewenvironment{itemize}{\begin{cvlist}}{\end{cvlist}}
% \newcounter{cvlistenumcounter}
% 
% \setlength{\topsep}{0pt}
% \setlength{\partopsep}{0pt}
% \newenvironment{cvlistenum}{
%   \setlength{\partopsep}{0pt}
% \begin{list}{\arabic{cvlistenumcounter}. }{\usecounter{cvlistenumcounter}\leftmargin=0em \itemindent=1.0em}
%   \setlength{\parskip}{0em}
%   \setlength{\parindent}{0em}
%   \setlength{\parsep}{0em}
%   \setlength{\itemsep}{0pt}
% }%
% {\end{list}}
% 
% \renewenvironment{enumerate}{\begin{cvlistenum}}{\end{cvlistenum}}
% no title
\renewcommand{\maketitle}{
\begin{minipage}{0.5\textwidth}  
  {\color{headings}\fontsize{18pt}{24pt}\selectfont {\textsc{\textbf{\myauthor}}}}
  \vfill
\end{minipage}
% \hfill
%  \begin{minipage}[t]{0.0in}
%    % dummy (needed here)
% \end{minipage}
\begin{minipage}[t]{0.5\textwidth}  
  {\footnotesize
  \href{mailto:\myemail}{\myemail} \hfill
  +1~714-813-8531\\
  \href{\myweb}{\myweb}
    \hfill
    \href{https://dla2.us/res}{https://dla2.us/res}
  }
  \vfill
\end{minipage}
}

% Page layout
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}
\fancyfoot{}
\makeatletter
 \fancyfoot[C]{{\scriptsize \@author\ -- \@title \ -- Page \thepage\ of\ \pageref{LastPage}}}
\makeatother

% suppress hyphenation for resumes; we do this because of automated
% keyword scanning systems which may miss appropriate keywords
\righthyphenmin=62
\lefthyphenmin=62
